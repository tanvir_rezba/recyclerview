package com.example.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.VHolder> {

    List<String> listName;
    List<String> ListNumber;
    Context context;

    public ViewAdapter(List<String> listName, List<String> listNumber, Context context) {
        this.listName = listName;
        ListNumber = listNumber;
        this.context = context;
    }

    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyleview,parent,false);
        VHolder vHolder = new VHolder(view);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VHolder holder, int position) {

        holder.name.setText(listName.get(position));
        holder.number.setText(ListNumber.get(position));
    }

    @Override
    public int getItemCount() {
        return listName.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView name,number;

        public VHolder(@NonNull View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.name);
            number=itemView.findViewById(R.id.number);


        }
    }
}
